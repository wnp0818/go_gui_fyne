package redis

import (
	"fmt"
	"go_gui/lib"
	"reflect"
)

// CheckHMSetVal 检测并转换HMSet所属设置的值
func CheckHMSetVal(val any) (map[string]any, error) {
	var (
		err        error
		setMapData map[string]any
	)
	valType := reflect.TypeOf(val)
	if valType.Kind() == reflect.Ptr {
		valType = valType.Elem()
	}
	switch valType.Kind() {
	case reflect.Struct:
		setMapData = lib.Convert.Struct2Map(val)
		// 转换出来的map为空
		if len(setMapData) == 0 {
			err = fmt.Errorf("redis.pipeline HMSet val cronvert to map is nil  val: 【%+v】\n", setMapData)
		}
	case reflect.Map:
		setMapData = val.(map[string]any)
	default:
		err = fmt.Errorf("redis.pipeline HMSet val type err!!\nvalType: %v \nerr-->%s\n", err, valType.Kind())
	}

	return setMapData, err
}
