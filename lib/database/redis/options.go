package redis

import (
	"go_gui/gameconfig"
)

// InitOptions 初始化redis配置
func InitOptions(ver string) gameconfig.Redis {
	switch ver {
	case "cross":
		return gameconfig.CrossServer.Redis
	default:
		return gameconfig.ServerConfig.Redis
	}

}
