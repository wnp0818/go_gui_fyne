package redis

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	"go_gui/logger"
)

// HGetAll 获取哈希key的所有kv
func (r *RedisStruct) HGetAll(key string, val any) error {
	StringStringMapCmd := r.Client.HGetAll(r.Context(), key)
	_mapData, _err := StringStringMapCmd.Result()

	// 有报错
	if _err != nil {
		_err = fmt.Errorf("redis(%s) HGetAll fail!!\nkey: %s\nerr-->%s", r.Ver, key, _err)
		logger.FyneLog.Warn(_err.Error())
		return _err
	}
	// 数据为空
	if len(_mapData) == 0 {
		return redis.Nil
	}

	_err = StringStringMapCmd.Scan(val)
	// 转换失败
	if _err != nil {
		_err = fmt.Errorf("redis(%s) HGetAll Convert!!\nkey: %s\nerr-->%s", r.Ver, key, _err)
		logger.FyneLog.Warn(_err.Error())
		return _err
	}

	return nil
}

// HMSet 设置哈希key的多个值
func (r *RedisStruct) HMSet(key string, val any) error {
	setMapData, err := CheckHMSetVal(val)
	if err != nil {
		err = fmt.Errorf("redis(%s) HMSet val err!!\nkey: %s  val: %v \nerr-->%s\n", r.Ver, key, err, val)
		logger.FyneLog.Warn(err.Error())
		return err
	}

	boolCmd := r.Client.HMSet(r.Context(), key, setMapData)
	_, err = boolCmd.Result()
	if err != nil {
		err = fmt.Errorf("redis(%s) HMSet fail!!\nkey: %s\nerr-->%s\n", r.Ver, key, err)
		logger.FyneLog.Warn(err.Error())
	}

	return err
}

// HDel 设置哈希key的多个值
func (r *RedisStruct) HDel(key string, field string) *redis.IntCmd {
	intCmd := r.Client.HDel(r.Context(), key, field)
	return intCmd
}
