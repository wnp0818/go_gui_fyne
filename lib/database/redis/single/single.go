package single

import (
	"github.com/go-redis/redis/v8"
	"go_gui/gameconfig"
)

type Client struct {
	*redis.Client
}

// NewSingleClient client
func NewSingleClient(options gameconfig.Redis) *Client {
	_client := &Client{
		Client: redis.NewClient(
			&redis.Options{
				Addr:     options.Address,
				Password: options.PassWord,
			},
		),
	}

	return _client
}
