package cluster

import (
	"github.com/go-redis/redis/v8"
	"go_gui/gameconfig"
)

type Client struct {
	*redis.ClusterClient
}

// NewClusterClient 集群client
func NewClusterClient(options gameconfig.Redis) *Client {
	_client := &Client{
		ClusterClient: redis.NewClusterClient(
			&redis.ClusterOptions{
				Addrs:    options.AddressList,
				Password: options.PassWord,
			},
		),
	}
	_client.ClusterClient.Ping(_client.ClusterClient.Context())
	return _client
}
