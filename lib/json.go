package lib

import (
	"bytes"
	jsoniter "github.com/json-iterator/go"
)

func Dumps(i interface{}) []byte {
	_data, _ := jsoniter.Marshal(i)
	return _data
}

func Loads(data []byte, i interface{}) error {
	d := jsoniter.NewDecoder(bytes.NewBuffer(data))
	//d.UseNumber()
	_err := d.Decode(i)
	return _err
}

// MarshalIndent 格式化输出 Prefix不支持随便传
func MarshalIndent(data any, prefix, indent string) string {
	_res := bytes.NewBuffer([]byte{})
	_e := jsoniter.NewEncoder(_res)
	_e.SetIndent(prefix, indent)
	_e.Encode(data)
	return _res.String()
}

// DefaultMarshalIndent 默认格式化输出
func DefaultMarshalIndent(data any) string {
	return MarshalIndent(data, "", "    ")
}
