package lib

import (
	"fmt"
	"go_gui/logger"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/anypb"
)

func Marshal(m proto.Message) []byte {
	msg, err := proto.Marshal(m)
	if err != nil {
		return nil
	}

	return msg
}

func Unmarshal(b []byte, m proto.Message) error {
	err := proto.Unmarshal(b, m)
	return err

}

// NewProtoAny new一个 anypb.Any
func NewProtoAny(protoData proto.Message) *anypb.Any {
	_anyData, _err := anypb.New(protoData)
	if _err != nil {
		logger.FyneLog.Warn(fmt.Sprintf("NewProtoAny error ->%s", _err))
	}

	return _anyData
}
