package timer

import (
	"fmt"
	"go_gui/logger"
	"runtime/debug"
)

type Job struct {
	F    Func   // 执行方法
	Args []any  // 方法参数
	Key  string // 定时器表示  模块名.定时器文件名
}

var NoChkMap = map[string]struct{}{}

func (j *Job) Run() {
	defer func() {
		if r := recover(); r != nil {
			errMsg := fmt.Sprintf(
				"Timer.Run error!!\napi:【%s】 args: %v\nerr--->: 【%s】\n%s\n",
				j.Key,
				j.Args,
				r.(error).Error(),
				debug.Stack(),
			)
			logger.TimerLog.Error(errMsg)
		}
	}()
	_res := j.F(j.Args...)

	if j.ChkRes(j.Key) {
		// 执行失败
		if _res != "success" {
			logger.Print("定时器【%s】执行失败!! msg-->%s", j.Key, _res)
		} else {
			// 执行成功
			logger.Print("定时器【%s】执行成功!!", j.Key)
		}
		fmt.Println()
	}

}

// ChkRes 是否检测执行结果
func (j *Job) ChkRes(key string) bool {
	_, ok := NoChkMap[key]
	if ok {
		return false
	}
	return true

}
