package main

import (
	"go_gui/game/module"
	"go_gui/lib/timer"
)

func main() {
	module.InitRoute()
	_timer := timer.NewTimer()
	_timer.Run("timer")

	select {}
}
