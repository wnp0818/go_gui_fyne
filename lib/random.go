package lib

import (
	"fmt"
	"math/rand"
	"time"
)

type RandStruct struct {
	dom *rand.Rand
}

var Rand RandStruct

func init() {
	Rand = RandStruct{
		dom: rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

// RandInt 随机一个数字
func (r RandStruct) RandInt(args ...int) int {
	switch len(args) {
	case 0:
		return r.dom.Int()
	case 1:
		return r.dom.Intn(args[0])
	case 2:
		min, max := args[0], args[1]
		if max-min < 0 {
			panic(fmt.Errorf("(%d %d): max < min", min, max))
		}

		return r.dom.Intn(max-min) + min
	}

	panic(fmt.Errorf("takes exactly 2 argument (%d given)", len(args)))
}
