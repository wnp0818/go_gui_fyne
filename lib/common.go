package lib

type CommonStruct struct {
	SysStruct
	ConvertStruct
	RandStruct
	TimeStruct
}

var Common = CommonStruct{
	Sys,
	Convert,
	Rand,
	Time,
}

// InMap 判断元素是否在map中
func InMap(key string, data map[string]any) bool {
	_, ok := data[key]
	return ok
}

// InSlice 判断元素是否在切片中
func InSlice[ele comparable](key ele, data []ele) bool {
	for _, val := range data {
		if val == key {
			return true
		}
	}

	return false
}
