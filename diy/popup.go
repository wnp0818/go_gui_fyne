package diy

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"time"
)

// ShowLabelInfo x秒后消失的label弹出窗口
func ShowLabelInfo(msg string, obj fyne.CanvasObject, showTime time.Duration) *widget.PopUp {
	_popUp := widget.NewPopUp(
		widget.NewLabel(msg),
		fyne.CurrentApp().Driver().CanvasForObject(obj),
	)
	// 当前选择的控件左上角坐标
	_popUp.ShowAtPosition(fyne.CurrentApp().Driver().AbsolutePositionForObject(obj))

	if showTime > 0 {
		go func() {
			time.Sleep(showTime)
			_popUp.Hide()
		}()
	}

	return _popUp
}

// ShowTextInfo x秒后消失的text弹出窗口
func ShowTextInfo(msg string, obj fyne.CanvasObject, showTime time.Duration) *widget.PopUp {
	_entry := widget.NewMultiLineEntry()
	_entry.Text = msg
	_popUp := widget.NewPopUp(
		_entry,
		fyne.CurrentApp().Driver().CanvasForObject(obj),
	)
	_popUp.Resize(fyne.NewSize(350, 95))
	// 当前选择的控件左上角坐标
	_popUp.ShowAtPosition(fyne.CurrentApp().Driver().AbsolutePositionForObject(obj))

	if showTime > 0 {
		go func() {
			time.Sleep(showTime)
			_popUp.Hide()
		}()
	}

	return _popUp
}

// ShowSysInfo 系统弹出窗口
func ShowSysInfo(msg string) {
	fyne.CurrentApp().SendNotification(&fyne.Notification{
		Content: msg,
	})
}
