package diy

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

/*
	如果MenuLabel用于widget.Table，label不能用指针，否则会导致tabel元素错乱
	Table底层复用的label，可能用于不同的坐标点
*/
type MenuLabel struct {
	widget.Label
	Menu *fyne.Menu
}

func NewMenuLabel(labelText string, menuItems ...*fyne.MenuItem) *MenuLabel {
	_menuLabel := &MenuLabel{
		Label: widget.Label{
			Text: labelText,
		},
		Menu: fyne.NewMenu("", menuItems...),
	}
	_menuLabel.ExtendBaseWidget(_menuLabel)
	return _menuLabel
}

// Tapped 标签点击事件
func (t *MenuLabel) Tapped(e *fyne.PointEvent) {
	if len(t.Menu.Items) > 0 {
		widget.ShowPopUpMenuAtPosition(t.Menu, fyne.CurrentApp().Driver().CanvasForObject(t), e.AbsolutePosition)
	}
}

// SetText 设置标签内容
func (t *MenuLabel) SetText(text string) {
	t.Label.Text = text
	t.Label.Refresh()
}

// Clear 清空
func (t *MenuLabel) Clear() {
	t.Label.Text = ""
	t.Menu.Items = nil
}

// SetMenuItems 设置按钮菜单选项
func (t *MenuLabel) SetMenuItems(menuItems ...*fyne.MenuItem) {
	_menuItems := make([]*fyne.MenuItem, len(menuItems))
	for idx, item := range menuItems {
		_menuItems[idx] = item
	}
	t.Menu.Items = _menuItems

}
