package diy

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/widget"
)

/*
	鼠标移入、出select
*/

type MySelect struct {
	*widget.Select
	PopShow   *widget.PopUpMenu // 悬浮显示select具体信息
	IsPopShow bool              // 是否悬浮显示select具体信息
}

func NewSelect(options []string, changed func(string)) *MySelect {
	s := &MySelect{
		Select: &widget.Select{
			OnChanged:   changed,
			Options:     options,
			PlaceHolder: "(Select one)",
		},
	}
	s.ExtendBaseWidget(s)
	return s
}

// MouseIn 鼠标移入
func (s *MySelect) MouseIn(event *desktop.MouseEvent) {
	s.Select.MouseIn(event)

	if s.IsPopShow {
		s.PopShow = widget.NewPopUpMenu(
			fyne.NewMenu("", fyne.NewMenuItem(s.Selected, func() {})),
			fyne.CurrentApp().Driver().CanvasForObject(s),
		)

		s.PopShow.ShowAtPosition(
			fyne.CurrentApp().Driver().AbsolutePositionForObject(s).
				Add(
					//fyne.NewPos(0, s.Size().Height-theme.InputBorderSize()),
					fyne.NewPos(0, s.Size().Height-30),
				),
		)
		s.PopShow.Resize(fyne.NewSize(s.Size().Width, s.PopShow.MinSize().Height))
	}

}

// MouseOut 鼠标移出
func (s *MySelect) MouseOut() {
	s.Select.MouseOut()
	if s.PopShow != nil {
		s.PopShow.Hide()
		s.PopShow = nil
	}
}
