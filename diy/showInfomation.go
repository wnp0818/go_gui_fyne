package diy

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"golang.org/x/image/colornames"
	"time"
)

// ShowInformation 自定义消息弹窗
func ShowInformation(text string, obj fyne.CanvasObject) {
	_popUpCloseChan := make(chan bool)
	_textLabel := &widget.Label{
		Text:      text,
		Alignment: fyne.TextAlignCenter,
	}
	_showInfoPopUp := widget.NewModalPopUp(
		container.NewVBox(
			_textLabel,
			container.NewGridWithColumns(
				3,
				layout.NewSpacer(),
				widget.NewButton("OK", func() {
					_popUpCloseChan <- true
				}),
				layout.NewSpacer(),
			),
		),
		fyne.CurrentApp().Driver().CanvasForObject(obj),
	)
	go func() {
		select {
		case <-_popUpCloseChan:
		}
		_showInfoPopUp.Hide()
	}()
	_showInfoPopUp.Show()
}

// ShowObjInformation 自定义载体消息弹窗
func ShowObjInformation(obj fyne.CanvasObject, canvasObj fyne.CanvasObject) {
	_popUpCloseChan := make(chan bool)
	_showInfoPopUp := widget.NewModalPopUp(
		container.NewVBox(
			canvasObj,
			container.NewGridWithColumns(
				3,
				layout.NewSpacer(),
				widget.NewButton("OK", func() {
					_popUpCloseChan <- true
				}),
				layout.NewSpacer(),
			),
		),
		fyne.CurrentApp().Driver().CanvasForObject(obj),
	)
	go func() {
		select {
		case <-_popUpCloseChan:
		}
		_showInfoPopUp.Hide()
	}()
	_showInfoPopUp.Show()
}

// ShowSleepInfor x秒后消失
func ShowSleepInfor(sleepTime time.Duration, text string) {
	drv := fyne.CurrentApp().Driver()
	if drv, ok := drv.(desktop.Driver); ok {
		w := drv.CreateSplashWindow()
		_text := &canvas.Text{
			Text:  text,
			Color: colornames.Red,
			TextStyle: fyne.TextStyle{
				Bold: true,
			},
			TextSize: 20,
		}
		w.SetContent(
			_text,
		)
		w.Show()

		go func() {
			time.Sleep(time.Second * sleepTime)
			w.Close()
		}()
	}
}
