package g

import (
	"fyne.io/fyne/v2"
	"go_gui/gameconfig"
	"go_gui/lib"
	"go_gui/lib/database/mongo"
)

var (
	C        lib.CommonStruct   // 通用方法模块
	Event    *lib.EventEmitter  // event
	Mdb      *mongo.MongoStruct // mongo
	CrossMdb *mongo.MongoStruct // cross mongo
	DebugMdb *mongo.MongoStruct // debug mongo

	MyApp            fyne.App    // 主app
	MainWin          fyne.Window // 主界面
	AccountManageWin fyne.Window // 账号管理界面
)

func init() {
	C = lib.Common
	Event = lib.GlobalEvent
}

func InitMongo(ver string, mongoOption gameconfig.MongoDb) {
	_mdb := mongo.InitMongo(mongoOption)
	switch ver {
	case "cross":
		CrossMdb = _mdb
	case "neiwang":
		DebugMdb = _mdb
	default:
		Mdb = _mdb
	}
}
