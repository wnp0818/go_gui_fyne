package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"go_gui/g"
	"go_gui/my_app/account_manage"
	"go_gui/my_app/window_time_modifier"

	"go_gui/my_theme"
)

func main() {
	NewApp()
	Run()
}

func NewApp() fyne.App {
	myApp := app.NewWithID("go_gui_fyne")
	// 设置中文字体
	myApp.Settings().SetTheme(&my_theme.MyTheme{})
	// 设置图标
	myApp.SetIcon(my_theme.ResourceMylogoPng)

	g.MyApp = myApp

	return myApp
}

func NewMainWin() fyne.Window {
	_mainWindow := g.MyApp.NewWindow("乐谷gui")
	_mainWindow.SetContent(
		//window_time_modifier.New(),
		container.NewGridWithColumns(
			3,
			widget.NewButton("乐谷账号管理", func() {
				account_manage.ShowWindow()
			}),
			widget.NewButton("Window时间修改器", func() {
				window_time_modifier.ShowWindow()
			}),
			widget.NewButton("敬请期待...", func() {

			}),
		),
	)
	g.MainWin = _mainWindow

	return _mainWindow
}

func Run() {
	_mainWin := NewMainWin()
	_mainWin.Resize(fyne.Size{
		Width:  500,
		Height: 200,
		//Height: 100,
	})
	_mainWin.CenterOnScreen()
	_mainWin.ShowAndRun()
}
