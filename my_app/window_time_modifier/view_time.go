package window_time_modifier

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"go_gui/diy"
	"go_gui/g"
	"go_gui/lib"
	"io"
	"net/http"
	"os/exec"
	"strings"
	"time"
)

var TimeView fyne.CanvasObject

type timeModifierStruct struct {
	Year   *widget.Entry
	Month  *widget.Entry
	Day    *widget.Entry
	Hour   *widget.Entry
	Minute *widget.Entry
	Second *widget.Entry
}

func NewTimeModifierStruct() *timeModifierStruct {
	return &timeModifierStruct{
		Year: &widget.Entry{
			PlaceHolder: "2022",
		},
		Month:  widget.NewEntry(),
		Day:    widget.NewEntry(),
		Hour:   widget.NewEntry(),
		Minute: widget.NewEntry(),
		Second: widget.NewEntry(),
	}
}

var TimeModifierStruct *timeModifierStruct

// GetDate  获取最新日期
func (t *timeModifierStruct) GetDate() string {
	_date := fmt.Sprintf("%s-%s-%s %s:%s:%s", t.Year.Text, t.Month.Text, t.Day.Text, t.Hour.Text, t.Minute.Text, t.Second.Text)
	return _date
}

// InitTimeViewFunc  时间初始化
func (t *timeModifierStruct) InitTimeViewFunc(nowTime ...int64) {
	var _time time.Time
	if len(nowTime) == 0 {
		_time = time.Now()
	} else {
		_time = time.Unix(nowTime[0], 0)
	}
	t.Year.Text = g.C.IntToString(_time.Year())
	t.Month.Text = g.C.IntToString(int(_time.Month()))
	t.Day.Text = g.C.IntToString(_time.Day())
	t.Hour.Text = g.C.IntToString(_time.Hour())
	t.Minute.Text = g.C.IntToString(_time.Minute())
	t.Second.Text = g.C.IntToString(_time.Second())
}

// GetNewBeiJinTime 获取最新北京时间
func GetNewBeiJinTime() (errorMsg string, nowTime int64) {
	url := "https://timeapi.io/api/Time/current/zone?timeZone=Asia/Shanghai"
	//url := "http://worldtimeapi.org/api/ip"
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Sprintf("获取最新时间失败[%s],请联系作者", err.Error()), 0
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "获取最新时间失败[ioutil.ReadAll error],请联系作者", 0
	}

	_res := make(map[string]any)
	err = lib.Loads(body, &_res)
	if err != nil {
		return "获取最新时间失败[lib.Loads error],请联系作者", 0
	}

	dateDtime, ok := _res["dateTime"]
	if !ok {
		return "获取最新时间失败[unixtime not exists],请联系作者", 0
	}

	dateDtime = strings.Split(dateDtime.(string), ".")[0]
	dateDtime = strings.Replace(dateDtime.(string), "T", " ", 1)

	nowTime = g.C.Date2Now(dateDtime.(string))

	return "", nowTime
}

// UpDateTimeViewFunc  更新为最新时间
func (t *timeModifierStruct) UpDateTimeViewFunc() {
	err, _nowTime := GetNewBeiJinTime()
	if err != "" {
		diy.ShowInformation(err, TimeView)
		return
	}

	t.InitTimeViewFunc(_nowTime)
	t.ModifierTimeViewFunc()
	TimeView.Refresh()
}

// ModifierTimeViewFunc  修改为当前时间
func (t *timeModifierStruct) ModifierTimeViewFunc() {
	_date := t.GetDate()
	_splitList := strings.Split(_date, " ")
	err := exec.Command("cmd", "/c", fmt.Sprintf("date %s && time %s", _splitList[0], _splitList[1])).Run()
	if err != nil {
		diy.ShowInformation("更新系统时间错误:请用管理员身份启动程序!", TimeView)
	}
}

// NewTimeView 时间日历
func NewTimeView() fyne.CanvasObject {
	if TimeView != nil {
		return TimeView
	}

	TimeModifierStruct = NewTimeModifierStruct()
	TimeModifierStruct.InitTimeViewFunc()
	TimeView = container.NewHBox(
		layout.NewSpacer(),
		TimeModifierStruct.Year,
		widget.NewLabel("年"),
		TimeModifierStruct.Month,
		widget.NewLabel("月"),
		TimeModifierStruct.Day,
		widget.NewLabel("日"),
		TimeModifierStruct.Hour,
		widget.NewLabel("时"),
		TimeModifierStruct.Minute,
		widget.NewLabel("分"),
		TimeModifierStruct.Second,
		widget.NewLabel("秒"),
		layout.NewSpacer(),
	)

	return TimeView
}
