package window_time_modifier

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"go_gui/g"
)

/*
	window时间修改
*/

// windowTimeModifierComp 时间修改组件
type windowTimeModifierComp struct {
	Win fyne.Window
}

func ShowWindow() {
	NewWindow().Show()
}

func NewWindow() fyne.Window {
	myWindow := g.MyApp.NewWindow("window时间修改器")
	myWindow.SetOnClosed(func() {
	})
	myWindow.Resize(fyne.Size{
		Width:  500,
		Height: 100,
	})
	myWindow.CenterOnScreen()
	myWindow.SetContent(New())

	return myWindow
}

func New() fyne.CanvasObject {
	_mainUi := container.NewVBox(
		NewTimeView(),
		//layout.NewSpacer(),
		container.NewHBox(
			layout.NewSpacer(),
			widget.NewButton("同步为最新时间", func() {
				TimeModifierStruct.UpDateTimeViewFunc()
			}),
			widget.NewButton("修改时间", func() {
				TimeModifierStruct.ModifierTimeViewFunc()
			}),
			layout.NewSpacer(),
		),
		//layout.NewSpacer(),
	)
	return _mainUi
}
