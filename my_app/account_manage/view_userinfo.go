package account_manage

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"go.mongodb.org/mongo-driver/bson"
	"go_gui/diy"
	"go_gui/g"
	"go_gui/lib"
	"strings"
)

// ShowUserInfoView 玩家信息视图
func ShowUserInfoView(uid string) fyne.Window {
	if uid == "" {
		diy.ShowInformation("请输入玩家uid", AccountComp.GameView)
		return nil
	}

	_userInfo := g.Mdb.FindOne("userinfo", bson.M{"uid": uid})
	if len(_userInfo) == 0 {
		diy.ShowInformation("查无此人", AccountComp.GameView)
		return nil
	}

	// 优先显示uid，binduid，lv，vip，rmbmoney
	_attrList := []string{"uid", "binduid", "lv", "vip", "rmbmoney", "lasttime"}
	var _fmtStr string
	for idx, k := range _attrList {
		if k == "lasttime" {
			_fmtStr += fmt.Sprintf("最后登录时间: %v", g.C.Now2Date(int64(_userInfo[k].(int32))))
		} else {
			_fmtStr += fmt.Sprintf("%s: %s", k, g.C.Any2String(_userInfo[k]))
		}
		if idx < len(_attrList)-1 {
			_fmtStr += "\n"
		}
		delete(_userInfo, k)
	}
	_topShowData := &widget.Entry{
		Text: _fmtStr,
		TextStyle: fyne.TextStyle{
			Bold: true,
		},
	}

	w := fyne.CurrentApp().NewWindow(fmt.Sprintf("玩家信息-%s", uid))
	_view := widget.NewMultiLineEntry()
	_view.Text = lib.DefaultMarshalIndent(_userInfo)
	// 点击重新Copy
	_copyButton := widget.NewButton("复制账号到内网", func() {
		w.Close()
		CopyUser(uid)
	})
	_copyButton.Importance = widget.HighImportance
	_mainUi := container.NewBorder(
		_topShowData, _copyButton, nil, nil,
		_view,
	)
	if strings.Contains(CurServerInfo.DbHost, "10.0.0.") {
		_copyButton.Hide()
	}
	w.SetContent(_mainUi)
	w.CenterOnScreen()
	w.Resize(fyne.NewSize(500, 600))
	w.Show()

	return w
}
