package account_manage

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"go_gui/diy"
	"go_gui/g"
)

// AddProjectView 增加项目
func AddProjectView() {
	_nameEntry := &widget.Entry{
		PlaceHolder: "项目名字",
	}
	_gameKeyEntry := &widget.Entry{
		PlaceHolder: "gametools唯一key(可不填)",
	}
	_debugNameEntry := &widget.Entry{
		PlaceHolder: "内网测试服名字(可不填)",
	}
	_debugHostEntry := &widget.Entry{
		PlaceHolder: "内网测试服host(可不填)",
	}
	_debugDbNameEntry := &widget.Entry{
		PlaceHolder: "内网测试服数据库(可不填)",
	}
	_debugSidEntry := &widget.Entry{
		PlaceHolder: "内网测试服sid(可不填,默认0(非0需填写))",
	}
	_form := diy.NewForm(
		AccountComp.GameView,
		func() {
			_name := _nameEntry.Text
			_gameKey := _gameKeyEntry.Text
			_debugName := _debugNameEntry.Text
			_debugHost := _debugHostEntry.Text
			_debugDbName := _debugDbNameEntry.Text
			_debugSid := _debugSidEntry.Text
			_gameInfo := &GameInfo{
				Name:        fmt.Sprintf("%s(add)", _name),
				GameKey:     _gameKey,
				Server2Info: make(map[string]*serverInfo),
				View:        baseView,
			}
			if _debugSid != "" {
				_gameInfo.DebugSid = g.C.String2Int(_debugSid)
			}
			if _debugHost != "" && _debugDbName != "" && _debugName != "" {
				_gameInfo.Server2Info[_debugName] = &serverInfo{
					Ver:    "dev",
					DbHost: _debugHost,
					DbName: _debugDbName,
				}
			}
			RegisterProject(_gameInfo)
			AccountComp.GameTree.Refresh()
		},
		func() string {
			_name := _nameEntry.Text
			if _name == "" {
				return "项目名字不能为空!"
			}

			_gameKey := _gameKeyEntry.Text
			//if _gameKey == "" {
			//	return "gametools唯一key不能为空"
			//}

			if _gameKey != "" && len(GetServerConfigList(_gameKey, "")) == 0 {
				return "gametools唯一key错误,请检查"
			}

			_debugName := _debugNameEntry.Text
			_debugHost := _debugHostEntry.Text
			_debugDbName := _debugDbNameEntry.Text
			if _debugHost != "" || _debugDbName != "" || _debugName != "" {
				if _debugHost == "" || _debugDbName == "" || _debugName == "" {
					return "内网测试服参数未全部填写"
				}
			}

			return "success"
		},
		"新增项目",
		true,
		diy.NewFormItem("", _nameEntry),
		diy.NewFormItem("", _gameKeyEntry),
		diy.NewFormItem("", _debugNameEntry),
		diy.NewFormItem("", _debugHostEntry),
		diy.NewFormItem("", _debugDbNameEntry),
		diy.NewFormItem("", _debugSidEntry),
	)
	_form.Resize(fyne.NewSize(500, 0))
	_form.Show()
}
