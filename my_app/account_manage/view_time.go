package account_manage

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"go_gui/g"
	"strings"
	"time"
)

var TimeView fyne.CanvasObject

// NewTimeView 时间日历
func NewTimeView() fyne.CanvasObject {
	if TimeView != nil {
		return TimeView
	}
	_curDate := &canvas.Text{TextSize: 29}
	_weekDay := &canvas.Text{TextSize: 29}
	_curTime := &canvas.Text{TextSize: 29}
	view := container.NewHBox(
		layout.NewSpacer(),
		_curDate,
		_weekDay,
		_curTime,
		layout.NewSpacer(),
	)

	_updateFunc := func() {
		_date := g.C.Date()
		_split := strings.Split(_date, " ")
		_curDate.Text = _split[0] + " "
		_curDate.Refresh()
		_weekDay.Text = g.C.GetWeekDayString() + " "
		_weekDay.Refresh()
		_curTime.Text = _split[1]
		_curTime.Refresh()
	}
	_updateFunc()

	go func() {
		_ticker := time.NewTicker(time.Second * 1)
		for {
			select {
			case <-_ticker.C:
				_updateFunc()
			}
		}
	}()

	TimeView = view
	return view
}
