package account_manage

import (
	"fyne.io/fyne/v2"
	"go_gui/diy"
	"go_gui/g"
	"go_gui/gameconfig"
)

func InitMongo(serverInfo *serverInfo) {
	g.InitMongo(
		serverInfo.Ver,
		gameconfig.MongoDb{
			Url:      serverInfo.Url,
			PoolSize: 10,
			DbName:   serverInfo.DbName,
		},
	)
}

func NewLoading(text string, size float32, parent ...fyne.CanvasObject) *diy.LoadingStruct {
	var _parent fyne.CanvasObject
	if len(parent) == 0 {
		_parent = AccountComp.GameMainView
	} else {
		_parent = parent[0]
	}
	return diy.NewLoading(text, size, _parent)
}
