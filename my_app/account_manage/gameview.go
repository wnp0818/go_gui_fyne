package account_manage

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

// baseView 基础视图
func baseView(gameInfo *GameInfo) fyne.CanvasObject {
	view := container.NewBorder(
		container.NewVBox(
			ServerInfoView(gameInfo),
			widget.NewSeparator(),
			ConvertView(),
			widget.NewSeparator(),
		),
		nil,
		nil,
		nil,
		RankView(),
	)

	return view
}
