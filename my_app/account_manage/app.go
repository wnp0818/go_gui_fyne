package account_manage

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"go_gui/diy"
	"go_gui/g"
	"go_gui/my_theme"
)

/*
	乐谷账号管理
*/

// accountComp 全局组件
type accountComp struct {
	Win                fyne.Window
	GameView           *fyne.Container
	GameMainView       *fyne.Container
	GameTree           *widget.Tree
	ServerSelect       *widget.Select  // 区服下拉框
	RankSelect         *widget.Select  // 排行榜下拉框
	SumMoneyBox        *fyne.Container // 总充值显示
	SearchBindUidEntry *widget.Entry   // 查询binduid输入框
	SearchUidEntry     *widget.Entry   // 查询uid输入框
	BindUidResEntry    *widget.Entry   // binduid结果展示框
	UidResEntry        *widget.Entry   // uid结果展示框
}

var (
	AccountComp     = new(accountComp)
	CurServerInfo   *serverInfo // 当前区服信息
	DebugServerInfo *serverInfo // 内网区服信息
	CurGameInfo     *GameInfo   // 区服metadata
)

func ShowWindow() {
	NewWindow().Show()
}

func NewWindow() fyne.Window {
	myWindow := g.MyApp.NewWindow("乐谷账号管理工具1.0")
	myWindow.SetOnClosed(func() {
	})
	AccountComp.Win = myWindow
	myWindow.Resize(fyne.Size{
		Width:  1458,
		Height: 800,
	})
	myWindow.CenterOnScreen()
	myWindow.SetContent(New())

	return myWindow
}

func New() fyne.CanvasObject {
	// 区服相关数据
	gameView := container.NewMax()
	AccountComp.GameView = gameView

	// 右侧界面
	gameMainView := container.NewBorder(NewTimeView(), nil, nil, nil, gameView)
	AccountComp.GameMainView = gameMainView

	// 左侧目录树
	_gameTree := NewGameTree(gameView)

	// 左侧image
	_image := initImage()

	// 工具
	_tool := container.NewHBox(
		addProject(),
	)

	// 主体ui
	_mainUi := container.NewHSplit(
		container.NewBorder(_tool, _image, nil, nil, _gameTree),
		gameMainView,
	)
	_mainUi.SetOffset(0.2)

	return _mainUi
}

func initImage() fyne.CanvasObject {
	pay50 := canvas.NewImageFromResource(my_theme.ResourcePay50Png)
	pay50.FillMode = canvas.ImageFillContain
	pay50.SetMinSize(fyne.NewSize(160, 160))
	_imageBox := container.NewHBox(
		&widget.Label{
			Text: "友\n情\n赞\n助\n(¬‿¬)",
			TextStyle: fyne.TextStyle{
				Bold: true,
			},
		},
		pay50,
	)
	addFriend := canvas.NewImageFromResource(my_theme.ResourceAddFriendPng)
	addFriend.FillMode = canvas.ImageFillContain
	addFriend.SetMinSize(fyne.NewSize(160, 160))
	_imageBox2 := container.NewHBox(
		&widget.Label{
			Text: "技\n术\n交\n流\n(^‿^)",
			TextStyle: fyne.TextStyle{
				Bold: true,
			},
		},
		addFriend,
	)

	return container.NewVBox(_imageBox, _imageBox2)
}

// addProject 新增项目
func addProject() fyne.CanvasObject {
	_icon := diy.NewIcon(theme.ContentAddIcon())
	_icon.SetClickFunc(func(e *fyne.PointEvent) {
		AddProjectView()
	})
	return _icon
}
