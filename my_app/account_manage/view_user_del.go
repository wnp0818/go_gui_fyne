package account_manage

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/widget"
	"go.mongodb.org/mongo-driver/bson"
	"go_gui/diy"
	"go_gui/g"
	"strings"
	"time"
)

// DelUser 删除账号
func DelUser(uid string) {
	w := fyne.CurrentApp().NewWindow(fmt.Sprintf("玩家账号清除-%s", uid))
	_showList := container.NewVBox()
	_VScroll := container.NewVScroll(_showList)
	_showCard := widget.NewCard("", "", _VScroll)

	_showUi := container.NewBorder(
		&canvas.Text{
			Text: fmt.Sprintf(" 账号所在数据库: %s:%s", DebugServerInfo.DbHost, DebugServerInfo.DbName),
			TextStyle: fyne.TextStyle{
				Bold: true,
			},
			TextSize: 20,
		},
		nil,
		nil,
		nil,
		container.NewGridWithColumns(1, _showCard),
	)
	_state := 0 // 0正在删除  1删除结束
	w.SetCloseIntercept(func() {
		if _state == 0 {
			diy.ShowInformation("正在清除账号数据!请等待清除结束", _showUi)
			return
		}
		w.Close()
	})
	w.SetContent(_showUi)
	w.CenterOnScreen()
	w.Resize(fyne.NewSize(500, 500))
	w.Show()

	appendBindText := func(text binding.String) {
		_showList.Objects = append(_showList.Objects, widget.NewLabelWithData(text))
		_VScroll.ScrollToBottom()
		_showList.Refresh()
	}
	//appendText := func(text string) {
	//	_showList.Objects = append(_showList.Objects, widget.NewLabel(text))
	//	_VScroll.ScrollToBottom()
	//	_showList.Refresh()
	//}

	go func() {
		_tableList := getTableList()
		for _, tableName := range _tableList {
			if strings.Contains(tableName, "system.profile") {
				continue
			}
			overChan := make(chan bool)
			_showText := binding.NewString()
			_showText.Set(fmt.Sprintf("正在清除表【%s】...", tableName))
			appendBindText(_showText)

			// Del等待loading
			go func(tableName string, overChan chan bool) {
				_useTime := 0
				_loopNum := 0
				textStrList := []string{
					fmt.Sprintf("正在清除表【%s】.    ", tableName),
					fmt.Sprintf("正在清除表【%s】..   ", tableName),
					fmt.Sprintf("正在清除表【%s】...  ", tableName),
				}
				_ticker := time.NewTicker(time.Millisecond * 500)
				defer _ticker.Stop()
				_curIdx := 0
				for {
					select {
					case <-_ticker.C:
						_loopNum++
						if _loopNum%2 == 0 {
							_useTime++
						}
						_showText.Set(textStrList[_curIdx] + fmt.Sprintf("耐心等待:%d秒 ⊙﹏⊙", _useTime))
						_curIdx++
						if _curIdx >= len(textStrList) {
							_curIdx = 0
						}
					case <-overChan:
						if _useTime > 3 {
							_showText.Set(fmt.Sprintf("正在清除表【%s】success!! ^o^y  耗时:%d秒", tableName, _useTime))
						} else {
							_showText.Set(fmt.Sprintf("正在清除表【%s】success!! └(^o^)┘", tableName))
						}
						return
					}
				}
			}(tableName, overChan)

			_filter := bson.M{
				"$or": []map[string]any{
					{"uid": uid},
					{"user_uid": uid},
				},
			}
			g.DebugMdb.Delete(tableName, _filter)
			overChan <- true
		}

		// 点击重新Copy
		_copyButton := widget.NewButton("重新Copy该账号", func() {
			w.Close()
			CopyUser(uid)
		})
		_copyButton.Importance = widget.HighImportance
		_successBox := container.NewHBox(
			widget.NewLabel("success!!"),
			_copyButton,
		)
		_showList.Objects = append(_showList.Objects, _successBox)
		_VScroll.ScrollToBottom()
		_showList.Refresh()

		diy.ShowInformation("账号清除成功!", _showUi)
		_state = 1
	}()

}
