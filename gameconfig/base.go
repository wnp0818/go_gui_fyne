package gameconfig

type Redis struct {
	PassWord    string   `json:"password"`  // 密码
	AddressList []string `json:"addresss"`  // redis集群地址
	Address     string   `json:"address"`   // redis单机地址
	IsCluster   bool     `json:"iscluster"` // 是否集群
}

type MongoDb struct {
	Url      string `json:"url"`      // mongo地址
	PoolSize uint64 `json:"poolsize"` // 连接池大小
	DbName   string `json:"dbname"`   // 数据库名
}

type ServerInfo struct {
	ServerName string `json:"servername"` // 区服名
	ServerId   int64  `json:"serverid"`   // 区服sid
}
