package main

import (
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/widget"
	"time"
)

func main() {
	a := app.New()
	w := a.NewWindow("Numerical")
	infProgress := widget.NewProgressBarInfinite()
	infProgress.Start()
	go func() {
		time.Sleep(time.Second * 10)
		infProgress.Stop()
	}()
	w.SetContent(infProgress)
	w.ShowAndRun()
}
